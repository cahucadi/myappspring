function loadProducts() {

    let request = sendRequest('producto/list', 'GET', '');

    let tableHTML = document.getElementById("table-content");
    tableHTML.innerHTML = "";

    request.onload = function () {

        let data = request.response;

        data.forEach( row => {

            tableHTML.innerHTML += `
            
            <tr>
                        <td> ${ row.idProducto } </td>
                        <td> ${ row.nombreProducto } </td>
                        <td class="col-right"> $ ${ row.valorCompra } </td>
                        <td class="col-right"> $ ${ row.valorVenta } </td>
                        <td class="col-right"> ${ row.cantidad } </td>
                        <td>
                            <a href="#" class="btn btn-sm btn-success">
                                <i class="bi bi-eye"></i>
                            </a>
                            <a href="#" class="btn btn-sm btn-warning">
                                <i class="bi bi-pencil"></i>
                            </a> 
                            <a href="#" class="btn btn-sm btn-danger">
                                <i class="bi bi-trash"></i>
                            </a>
                        </td>
            </tr>
            
            `;

        });

    }

    request.onerror = function(){
        
        let tableHTML = document.getElementById("table-content");
        tableHTML.innerHTML = "";

        tableHTML.innerHTML += `

        <tr>
            <td colspan="6">
                Error al cargar los productos
            </td>
        </tr>

        `;
    }


}