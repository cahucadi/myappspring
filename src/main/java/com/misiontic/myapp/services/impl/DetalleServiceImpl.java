/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.myapp.services.impl;

import com.misiontic.myapp.dao.DetalleDao;
import com.misiontic.myapp.models.Detalle;
import com.misiontic.myapp.services.DetalleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author cahuc
 */
@Service
public class DetalleServiceImpl implements DetalleService{
    
    @Autowired
    DetalleDao detalleDao;

    @Override
    public Detalle save(Detalle detalle) {
        return detalleDao.save( detalle );
    }

    @Override
    public void delete(Integer id) {
        detalleDao.deleteById(id);
    }

    @Override
    public Detalle findById(Integer id) {
        return detalleDao.findById(id).orElse(null);
    }

    @Override
    public List<Detalle> findAll() {
        return (List<Detalle>) detalleDao.findAll();
    }
    
}
