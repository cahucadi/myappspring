/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.myapp.services.impl;

import com.misiontic.myapp.dao.ProductoDao;
import com.misiontic.myapp.models.Producto;
import com.misiontic.myapp.services.ProductoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author cahuc
 */
@Service
public class ProductoServiceImpl implements ProductoService{
    
    @Autowired
    ProductoDao productoDao;

    @Override
    public Producto save(Producto producto) {
        return productoDao.save( producto);
    }

    @Override
    public void delete(Integer id) {
        productoDao.deleteById(id);
    }

    @Override
    public Producto findById(Integer id) {
        return productoDao.findById(id).orElse(null);
    }

    @Override
    public List<Producto> findAll() {
        return (List<Producto>) productoDao.findAll();
    }

    @Override
    public Producto findByProductName( String name) {
        return productoDao.findByNombreProducto(name);
    }
    
}
