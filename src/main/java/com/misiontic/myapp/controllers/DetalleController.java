/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.myapp.controllers;

import com.misiontic.myapp.models.Detalle;
import com.misiontic.myapp.models.Producto;
import com.misiontic.myapp.services.DetalleService;
import com.misiontic.myapp.services.ProductoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cahuc
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/detalle")
public class DetalleController {

    @Autowired
    private DetalleService detalleService;

    @GetMapping(value = "/list")
    public List<Detalle> consultarTodo() {
        return detalleService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public Detalle consultarPorId(@PathVariable Integer id) {
        return detalleService.findById(id);
    }

    @PostMapping(value = "/")
    public ResponseEntity<Detalle> agregar(@RequestBody Detalle detalle) {
        Detalle resultado = detalleService.save(detalle);
        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Detalle> editar(@RequestBody Detalle nuevo) {

        Detalle actual = detalleService.findById(nuevo.getIdDetalle());

        if (actual == null) {
            return new ResponseEntity<>(actual, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {

            detalleService.save(actual);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Detalle> eliminar(@PathVariable Integer id) {
        Detalle resultado = detalleService.findById(id);

        if (resultado == null) {
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            detalleService.delete(id);
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        }
    }
}
