/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.myapp.controllers;

import com.misiontic.myapp.models.Producto;
import com.misiontic.myapp.services.ProductoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cahuc
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/producto")
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @GetMapping(value = "/list")
    public List<Producto> consultarTodo() {
        return productoService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public Producto consultarPorId(@PathVariable Integer id) {
        return productoService.findById(id);
    }
    
    @GetMapping(value = "/list/name/{name}")
    public Producto consultarPorNombre(@PathVariable String name) {
        return productoService.findByProductName(name);
    }

    @PostMapping(value = "/")
    public ResponseEntity<Producto> agregar(@RequestBody Producto producto) {
        Producto resultado = productoService.save(producto);
        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Producto> editar(@RequestBody Producto nuevo) {

        Producto actual = productoService.findById(nuevo.getIdProducto());

        if (actual == null) {
            return new ResponseEntity<>(actual, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            actual.setNombreProducto(nuevo.getNombreProducto());
            actual.setValorCompra(nuevo.getValorCompra());
            actual.setValorVenta(nuevo.getValorVenta());
            actual.setCantidad(nuevo.getCantidad());
            productoService.save(actual);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Producto> eliminar(@PathVariable Integer id) {
        Producto resultado = productoService.findById(id);

        if (resultado == null) {
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            productoService.delete(id);
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        }
    }
}
