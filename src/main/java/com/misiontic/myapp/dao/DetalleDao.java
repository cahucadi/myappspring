/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.myapp.dao;

import com.misiontic.myapp.models.Detalle;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author cahuc
 */
public interface DetalleDao extends CrudRepository<Detalle, Integer>{
    
}
