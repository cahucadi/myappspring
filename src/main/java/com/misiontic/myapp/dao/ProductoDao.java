/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.myapp.dao;

import com.misiontic.myapp.models.Producto;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author cahuc
 */
@Repository
public interface ProductoDao extends JpaRepository<Producto, Integer>{
    
    Producto findByNombreProducto( String nombreProducto );
    
}
