/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.myapp.dao;

import com.misiontic.myapp.models.Transaccion;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author cahuc
 */
public interface TransaccionDao extends CrudRepository<Transaccion, Integer>{
    
}
